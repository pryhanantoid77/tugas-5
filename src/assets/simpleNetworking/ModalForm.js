export const Modalform = ({navigation, route}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(true);
  var dataMobil = route.params;

  useEffect(() => {
    console.log('log123', dataMobil);
    if (route.params) {
      const data = route.params;
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
    }
  }, []);

  const postData = async () => {
    if (namaMobil == '') {
      alert('Nama Mobil Tidak Boleh Kosong');
    } else if (totalKM == '') {
      alert('Total Kilometer Tidak Boleh Kosong');
    } else if (hargaMobil == '') {
      alert('Harga Mobil Tidak Boleh Kosong');
    } else if (hargaMobil < 100000000) {
      alert('Harga Mobil Kurang dari 100JT');
    } else {
      setIsLoading(true);
      const body = [
        {
          title: namaMobil,
          harga: hargaMobil,
          totalKM: totalKM,
          unitImage:
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
        },
      ];

      try {
        const response = await fetch(`${BASE_URL}mobil`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: TOKEN,
          },
          body: JSON.stringify(body),
        });

        const result = await response.json();
        console.log('Success:', result);
        alert('Data Mobil berhasil ditambahkan');
        navigation.navigate('Home');
      } catch (error) {
        console.error('Error:', error);
      }
      setIsLoading(false);
    }
  };

  const editData = async () => {
    setIsLoading(true);
    const body = [
      {
        _uuid: dataMobil._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];
    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      console.log('Success:', result);
      alert('Data Mobil berhasil dirubah');
      navigation.navigate('Home');
    } catch (error) {
      console.error('Error:', error);
    }
    setIsLoading(false);
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Modal
        // animationType={'slide'}
        transparent={true}
        visible={isLoading}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        {/*All views of Modal*/}
        {/*Animation can be slide, slide, none*/}
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.2)',
          }}>
          <View
            style={{
              width: '80%',
              height: 300,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 10,
              marginTop: 80,
              marginHorizontal: 40,
            }}>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
      <Modal
        // animationType={'slide'}
        transparent={true}
        visible={showModal}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        {/*All views of Modal*/}
        {/*Animation can be slide, slide, none*/}
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.2)',
          }}>
          <View
            style={{
              width: '80%',
              height: '60%',
              backgroundColor: 'white',
              // justifyContent: 'center',
              // alignItems: 'center',
              borderRadius: 10,
              marginTop: 80,
              marginHorizontal: 40,
            }}>
            <ScrollView>
              <View
                style={{
                  // width: '100%',
                  padding: 15,
                }}>
                <View>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Nama Mobil
                  </Text>
                  <TextInput
                    value={namaMobil}
                    onChangeText={text => setNamaMobil(text)}
                    placeholder="Masukkan Nama Mobil"
                    style={styles.txtInput}
                  />
                </View>
                <View style={{marginTop: 20}}>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Total Kilometer
                  </Text>
                  <TextInput
                    value={totalKM}
                    onChangeText={text => setTotalKM(text)}
                    placeholder="contoh: 100 KM"
                    style={styles.txtInput}
                  />
                </View>
                <View style={{marginTop: 20}}>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Harga Mobil
                  </Text>
                  <TextInput
                    value={hargaMobil}
                    onChangeText={text => setHargaMobil(text)}
                    placeholder="Masukkan Harga Mobil"
                    style={styles.txtInput}
                    keyboardType="number-pad"
                  />
                </View>
                <TouchableOpacity
                  onPress={() => (dataMobil ? editData() : postData())}
                  //   onPress={() => <Loading />}
                  style={styles.btnAdd}>
                  <Text style={{color: '#fff', fontWeight: '600'}}>
                    {dataMobil ? 'Edit Data' : 'Tambah Data'}
                  </Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    </View>
  );
};
